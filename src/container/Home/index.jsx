import React from 'react';
import Button from 'components/Button';
import AppLayout from 'components/layout/appLayout';

export default function Home() {
  return (
    <AppLayout>
      <div className="container">
        <div className="row">
          <div className="col-md-3">
            <Button>Play1</Button>
          </div>
          <div className="col-md-3">
            <Button $type="brown" $width="100px">
              Play2
            </Button>
          </div>
          <div className="col-md-3">
            <Button $type="white"  $width="120px">Play3</Button>
          </div>
          <div className="col-md-3">
            <Button $type="yellow" $width="150px">Play4</Button>
          </div>
        </div>
      </div>
    </AppLayout>
  );
}
