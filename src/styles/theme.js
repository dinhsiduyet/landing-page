const fontSize = {
  xsm: '16px',
  sm: '17px',
  base: '18px',
  md: '20px',
  lg: '24px',
  xl: '33px',
  xxl: '50px',
  xxxl: '84px',
};

const colors = {
  black: '#000000',
  nearBlack: '#212832',
  white: '#fff',
  blue: '#181E4B',
  blue2: '#14183E',
  blue3: '#1E1D4C',
  blue4: '#5E6282',
  blue5: '#39425D',
  linearBlue: '#747DEF',
  yellow: 'yellow',
  brown: '#DF6951',
  brown2: '#FF946D',
  grey1: '#686D77',
  grey2: '#D5D6DB',
  border: '#84829A',
};
const fontWeight = {
  regular: '400',
  medium: '500',
  bold: '600',
  extraBold: '700',
};
const borderRadius = {
  xsm: '4px',
  sm: '5px',
  base:'9px',
  md: '10px',
  xl: '36px',
};
const boxShadow = {
  base: '0px 4px 4px rgba(0, 0, 0, 0.25)',
  wrapBox: '0px 8px 6px 0px #000000',
  xl: '0px 100px 80px rgba(0, 0, 0, 0.02), 0px 64.8148px 46.8519px rgba(0, 0, 0, 0.0151852), 0px 38.5185px 25.4815px rgba(0, 0, 0, 0.0121481), 0px 20px 13px rgba(0, 0, 0, 0.01), 0px 8.14815px 6.51852px rgba(0, 0, 0, 0.00785185), 0px 1.85185px 3.14815px rgba(0, 0, 0, 0.00481481)',
};

export default {
  fontSize,
  fontWeight,
  colors,
  borderRadius,
  boxShadow,
};
