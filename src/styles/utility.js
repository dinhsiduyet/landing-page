import {css} from 'styled-components';

export const utility = css`
  .bg-graylight {
    background-color: ${({theme}) => theme.colors.gray2};
  }
  .bg-white2 {
    background-color: ${({theme}) => theme.colors.white2};
  }
`;
