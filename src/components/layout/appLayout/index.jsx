import React from 'react';
import * as S from './styled';
import Header from 'components/Header';
import Footer from 'components/Footer';

function AppLayout({children}) {
  return (
    <S.Wrapper>
      <Header />
      <S.Content>{children}</S.Content>
      <Footer />
    </S.Wrapper>
  );
}
export default AppLayout;
