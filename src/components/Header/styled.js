import styled from 'styled-components';
import {NavLink,Link} from 'react-router-dom';
import {media} from 'styles/breakPoints';
export const WrapHeader = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 48px 0px;
`;

export const Header = styled.ul`
  display: flex;
  align-items: center;
  ${media.phone`
    display:none;
  `}
`;
export const WrapHamberger = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;
export const Hamberger = styled.div`
  cursor: pointer;
`;

export const MenuItem = styled.li`
  display: flex;
  align-item: center;
  padding: 0px 23px;
  position: relative;
`;

export const MenuLink = styled(NavLink)`
  color: ${({theme}) => theme.colors.nearBlack};
  font-size: ${({theme}) => theme.fontSize.sm};
  font-weight: ${({theme}) => theme.fontWeight.regular};
  line-height:1.4;
  white-space: nowrap;
  transition:none;

  &:hover {
    color: ${({theme}) => theme.colors.linearBlue};
  }

  &.active {
    border-bottom: 2px solid ${({theme}) => theme.colors.linearBlue};
    color: ${({theme}) => theme.colors.linearBlue};
  }
`;

export const LogoWrapper = styled(Link)`
  
`;

export const LangWrapper = styled.span`
  cursor:pointer;
  user-select:none;
`;