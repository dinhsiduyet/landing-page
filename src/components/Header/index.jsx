import React, {useState, useCallback} from 'react';

import * as S from './styled';

import MainLogo from 'images/main-logo.png';
import IconDropDown from 'images/icon-dropdown.svg';
import Button from 'components/Button';
import SidebarMobile from 'components/SidebarMobile';
import PopOver from 'components/PopOver';
import {ReactComponent as IconHamburger} from 'images/icon-hamburger.svg';
export default function Header() {
  const [isShowSidebar, setIsShowSidebar] = useState(false);
  const [isShowPopOver, setIsShowPopOver] = useState(false);
  const handleCloseSideBar = useCallback(() => {
    setIsShowSidebar(false);
  }, []);

  function menuToggle() {
    setIsShowSidebar(!isShowSidebar);
  }

  function handlePopOverToggle(e) {
    e.preventDefault();
    setIsShowPopOver(!isShowPopOver);
  }

  return (
    <>
      <div className="container">
        <S.WrapHeader>
          <SidebarMobile isShow={isShowSidebar} onClose={handleCloseSideBar} />
          <S.WrapHamberger className="d-lg-none">
            <S.Hamberger>
              <IconHamburger onClick={menuToggle} />
            </S.Hamberger>
          </S.WrapHamberger>
          <S.LogoWrapper to="">
            <img src={MainLogo} alt="logo" />
          </S.LogoWrapper>
          <S.Header>
            <S.MenuItem>
              <S.MenuLink activeClassName="active" to="/" exact>
                Desitnations
              </S.MenuLink>
            </S.MenuItem>
            <S.MenuItem>
              <S.MenuLink to="/hotel">Hotels</S.MenuLink>
            </S.MenuItem>
            <S.MenuItem>
              <S.MenuLink to="/flight">Flights</S.MenuLink>
            </S.MenuItem>
            <S.MenuItem>
              <S.MenuLink to="/booking">Bookings</S.MenuLink>
            </S.MenuItem>
            <S.MenuItem>
              <S.MenuLink to="/login">Sign Up</S.MenuLink>
            </S.MenuItem>
          </S.Header>
          <S.Header>
            <Button $type="white" size="sm" className="me-4 d-none d-lg-block">
              Login
            </Button>

            <S.MenuItem
              className="d-none d-lg-block"
              onClick={handlePopOverToggle}
            >
              <S.LangWrapper>
                EN
                <img src={IconDropDown} alt="" className="ms-2" />
              </S.LangWrapper>
              <PopOver isShow={isShowPopOver} />
            </S.MenuItem>
          </S.Header>
        </S.WrapHeader>
      </div>
    </>
  );
}
