import styled from 'styled-components';

export const Heading2 = styled.h2`
  font-size: ${({theme}) => theme.fontSize.xxl};
`;
export const Heading3 = styled.h3`
  font-size: ${({theme}) => theme.fontSize.xl};
`;
