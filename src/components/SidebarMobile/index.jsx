import React, {useState} from 'react';
import * as S from './styled';
import {Link} from 'react-router-dom';
import Button from 'components/Button';
import PopOver from 'components/PopOver';
import IconDropDown from 'images/icon-dropdown.svg';
import {ReactComponent as IconClose} from 'images/icon-close.svg';
export default function SidebarMobile({isShow = false, onClose}) {
  const [isShowPopOver, setIsShowPopOver] = useState(false);
  function handlePopOverToggle(e) {
    e.preventDefault();
    setIsShowPopOver(!isShowPopOver);
  }
  return (
    <>
      <S.BackDrop $isShow={isShow} onClick={onClose}></S.BackDrop>
      <S.SidebarMobile $isShow={isShow}>
        <S.Inner>
          <S.WrapIconClose onClick={onClose}>
            <IconClose />
          </S.WrapIconClose>

          <S.MenuItem>
            <S.MenuLink activeClassName="active" to="/" exact>
              Desitnations
            </S.MenuLink>
          </S.MenuItem>
          <S.MenuItem>
            <S.MenuLink to="/hotel">Hotels</S.MenuLink>
          </S.MenuItem>
          <S.MenuItem>
            <S.MenuLink to="/flight">Flights</S.MenuLink>
          </S.MenuItem>
          <S.MenuItem>
            <S.MenuLink to="/booking">Bookings</S.MenuLink>
          </S.MenuItem>
          <S.MenuItem>
            <S.MenuLink to="/login">Login</S.MenuLink>
          </S.MenuItem>

          <Link to="" className="d-block d-lg-none">
            <Button $type="white" size="sm" className="me-4">
              Sign Up
            </Button>
          </Link>
          <S.MenuItem
            className="d-block d-lg-none"
            onClick={handlePopOverToggle}
          >
            <S.LangWrapper>
              EN
              <img src={IconDropDown} alt="" className="ms-2" />
            </S.LangWrapper>
            <PopOver isShow={isShowPopOver} />
          </S.MenuItem>
        </S.Inner>
      </S.SidebarMobile>
    </>
  );
}
