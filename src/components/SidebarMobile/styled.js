import styled, {css} from 'styled-components';
import {NavLink} from 'react-router-dom';
import {mediaMin} from 'styles/breakPoints';
export const BackDrop = styled.div`
  width: 100vw;
  height: 100vh;
  background: rgba(0, 0, 0, 0.2);
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  z-index: 4;

  ${({$isShow}) =>
    !$isShow &&
    css`
      display: none;
    `}

  ${mediaMin.biggerTablet`
    display:none;

    `}
`;

export const SidebarMobile = styled.div`
  width: 100%;
  height: 100%;
  position: fixed;
  max-width: 380px;
  top: 0;
  left: 0;
  right: 0;
  z-index: 6;
  padding-right: 20px;
  margin-left: -380px;
  transition: all 0.4s ease-in-out;

  ${({$isShow}) =>
    $isShow &&
    css`
      margin-left: 0px;
    `}

  ${mediaMin.biggerTablet`
    display:none;

    `}
`;

export const Inner = styled.div`
  height: 100vh;
  border-radius: 0 4px 4px 0;
  background-color: white;
  padding: 24px 24px 24px 55px;
`;

export const WrapIconClose = styled.div`
  width: 30px;
  height: 30px;
  cursor: pointer;
`;

export const MenuItem = styled.li`
  display: flex;
  align-item: center;
  padding: 30px 23px;
`;

export const MenuLink = styled(NavLink)`
  color: ${({theme}) => theme.colors.nearBlack};
  font-size: ${({theme}) => theme.fontSize.sm};
  font-weight: ${({theme}) => theme.fontWeight.regular};
  line-height: 1.4;
  white-space: nowrap;
  transition: none;

  &:hover {
    color: ${({theme}) => theme.colors.linearBlue};
  }

  &.active {
    border-bottom: 2px solid ${({theme}) => theme.colors.linearBlue};
    color: ${({theme}) => theme.colors.linearBlue};
  }
`;

export const LangWrapper = styled.span`
  cursor:pointer;
  user-select:none;
`;