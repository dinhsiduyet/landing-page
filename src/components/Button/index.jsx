import React from 'react';

import * as S from './styled';
function Button({
  $type,
  children,
  size = 'base',
  width,
  maxWidth,
  height,
  ...rest
}) {
  if ($type === 'brown') {
    return (
      <S.ButtonBrown
        type="button"
        $size={size}
        $width={width}
        $maxWidth={maxWidth}
        $height={height}
        {...rest}
      >
        {children}
      </S.ButtonBrown>
    );
  }

  if ($type === 'white') {
    return (
      <S.ButtonWhite
        type="button"
        $size={size}
        $width={width}
        $maxWidth={maxWidth}
        $height={height}
        {...rest}
      >
        {children}
      </S.ButtonWhite>
    );
  }

  if ($type === 'yellow') {
    return (
      <S.ButtonYellow
        type="button"
        $size={size}
        $width={width}
        $maxWidth={maxWidth}
        $height={height}
        {...rest}
      >
        {children}
      </S.ButtonYellow>
    );
  }

  return (
    <>
      <S.Button
        type="button"
        $type={$type}
        $size={size}
        $width={width}
        $maxWidth={maxWidth}
        $height={height}
        {...rest}
      >
        {children}
      </S.Button>
    </>
  );
}
export default Button;
