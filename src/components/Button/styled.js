import styled, {css} from 'styled-components';

export const Button = styled.button`
  padding: 10px 20px;
  border: none;
  outline: none;
  display: flex;
  align-items: center;
  text-align: center;
  justify-content: center;
  cursor: pointer;
  border-radius: ${({theme}) => theme.borderRadius.xsm};

  ${({$size}) =>
    $size === 'sm' &&
    css`
      font-size: ${({theme}) => theme.fontSize.sm};
      height: 40px;
    `}

  ${({$size}) =>
    $size === 'base' &&
    css`
      font-size: ${({theme}) => theme.fontSize.base};
      height: 60px;
      border-radius: ${({theme}) => theme.borderRadius.md};
    `}

 ${({$width}) =>
    $width &&
    css`
      width: ${$width};
      padding: unset;
    `}

 ${({$maxWidth}) =>
    $maxWidth &&
    css`
      width: ${$maxWidth};
      padding: unset;
    `}
`;

export const ButtonWhite = styled(Button)`
  background-color: ${({theme}) => theme.colors.white};
  border: 1px solid ${({theme}) => theme.colors.border};
`;

export const ButtonYellow = styled(Button)`
  background-color: ${({theme}) => theme.colors.yellow};
`;

export const ButtonBrown = styled(Button)`
  background-color: ${({theme}) => theme.colors.brown};
`;
