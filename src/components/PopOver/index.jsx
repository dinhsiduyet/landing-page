import React from 'react';

import * as S from './styled';

export default function PopOver({isShow = false}) {
  return (
    <>
      <S.WrapPopOver $isShow={isShow}></S.WrapPopOver>
    </>
  );
}
