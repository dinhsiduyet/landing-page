import styled, {css} from 'styled-components';

export const WrapPopOver = styled.div`
  width: 100%;
  max-width: 230px;

  padding: 15px;
  background: ${({theme}) => theme.colors.black};
  border-radius: ${({theme}) => theme.borderRadius.sm};
  position: absolute;
  z-index:20;
 
  ${({$isShow}) =>
    !$isShow &&
    css`
      display: none;
    `};
`;
