import React from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Home from 'container/Home';
import SignIn from 'components/SignUp';
export default function MyRouter() {
  return (
    <>
      <Router>
        <Switch>
          <Route path="/" exactpath="true">
            <Home />
          </Route>
          <Route path="/product"></Route>
          <Route path="/sign-in">
            <SignIn />
          </Route>
        </Switch>
      </Router>
    </>
  );
}
